#!/bin/bash

#E-mail networksquad@tutanota.com
#Donate XMR 4BHyqPcSyNMRiXp9NyQKXBTAYXm2UHhzqZ1821zcy2arWnXSGSpSNvoWDBYTFbSADP5sxjzH6DZBnBrYTZfMpu6vRs8Qm2X

/bin/echo  -e "                                \033[33m LETs GO!!!!"
/usr/bin/apt-get  -y install figlet > /dev/null &&
figlet "Network Squad" && figlet  -f digital "                           presents"
echo  -e
echo "         ======================================================="
/bin/echo  -e "             \033[42m  AUTOMATIC INSTALLATION PRIVATE ACCESS POINT \033[40m"
echo "         ======================================================="
sleep 10
echo -e
echo -e
/bin/echo  -e " \033[32m Repository Update"
/usr/bin/apt-get  -y update  &&
sleep 10
/bin/echo -e  "\033[33m  OK!"
echo -e
/bin/echo  -e "\033[32m Software Installation"
/usr/bin/apt-get  -y install mc nano tor dnsmasq hostapd && wget http://www.figlet.org/fonts/moscow.flf > /dev/null
sleep 10
/bin/echo -e  "\033[33m  OK!"
echo -e
/bin/echo -e " \033[32m \033[1m Network configuration"
sleep 10
/bin/echo -e " \033[32m Forwarding Setup"
sleep 10
/bin/sed -i '/net.ipv4.ip_forward=1/s/^#//g' /etc/sysctl.conf
/bin/sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
/bin/echo -e  "\033[33m OK!"
echo -e
/bin/echo -e " \033[32m Firewall Setup"
/bin/mkdir /etc/iptables
sleep 10
/sbin/iptables  -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT

/sbin/iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT

/sbin/iptables -t nat -A PREROUTING -i wlan0 -p tcp -m tcp --dport 22 -j REDIRECT --to-ports 22

/sbin/iptables -t nat -A PREROUTING -i wlan0 -p udp -m udp --dport 53 -j REDIRECT --to-ports 53

/sbin/iptables -t nat -A PREROUTING -i wlan0 -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j REDIRECT --to-ports 9050

/sbin/iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

/bin/sh -c "iptables-save > /etc/iptables/iptables.ipv4.nat"

/bin/echo -e  "\033[33m  OK!"
echo -e
/bin/echo -e " \033[32m Configure Interfaces"
sleep 10
cp /etc/network/interfaces /etc/network/interfaces.orig
/bin/cat > /etc/network/interfaces << interfaces

auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp

allow-hotplug wlan0
iface wlan0 inet static
   address 10.10.10.1
   netmask 255.255.255.0

up iptables-restore  < /etc/iptables/iptables.ipv4.nat

interfaces

/bin/echo -e  "\033[33m OK!"
echo -e
/bin/echo -e " \033[32m  DHCP Setup"
/bin/cat >/etc/dnsmasq.conf <<dnsmasq_config
interface = wlan0
dhcp-authoritative
dhcp-range = 10.10.10.10, 10.10.10.20, 255.255.255.0, 24h
server = 8.8.8.8#53
server = 8.8.4.4#53
dnsmasq_config

#/usr/sbin/service dnsmasq start
#/usr/sbin/update-rc.d dnsmasq enable
#/usr/sbin/service dnsmasq restart
sleep 10
/bin/echo -e  "\033[33m  OK!"
echo -e
/bin/echo -e " \033[32m Setting an Access Point"
/bin/cat > /etc/hostapd/hostapd.conf << hostapd.conf

interface=wlan0
driver=nl80211
ssid=PutinKhuilo
hw_mode=g
channel=11
ieee80211n=1
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=networksquad
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
hostapd.conf

/bin/cp /etc/default/hostapd /etc/default/hostapd.orig
/bin/cat > /etc/default/hostapd << hostapd
DAEMON_CONF="/etc/hostapd/hostapd.conf"
hostapd

/bin/systemctl enable hostapd
/bin/systemctl unmask hostapd
/bin/systemctl enable hostapd
/bin/systemctl restart hostapd

sleep 10
/bin/echo -e  "\033[33m OK!"
echo -e
/bin/echo  -e " \033[32m Tor Setup"
/bin/cat >/etc/tor/torrc <<tor_config

ControlPort 9051
HashedControlPassword 16:872860B76453A77D60CA2BB8C1A7042072093276A3D701AD684053EC4C
CookieAuthentication 1
VirtualAddrNetwork 172.16.0.0/12
AutomapHostsSuffixes .onion, .exit
AutomapHostsOnResolve 1
ExcludeNodes {ru}, {ua}, {by}
TransPort 10.10.10.1:9050
DNSPort 10.10.10.1:53
tor_config

/bin/systemctl start tor
/bin/systemctl enable tor
sleep 10
/bin/echo -e  "\033[33m OK!"
 echo -e
/bin/echo -e "\033[31m \033[5m"

/bin/echo -e "\033[31m \033[5m"

figlet -f moscow.flf "  v bor/be obretew/ t| pravo svoe!"
/usr/sbin/service networking restart
sleep 10
echo  FINISH!!!!
sleap 5
echo REBOOT!!!!
/sbin/reboot
exit
